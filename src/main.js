import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vWow from 'v-wow'
// import VueWow from 'vue-wow'

// Vue.use(VueWow);
Vue.use(vWow);

// require('vue2-animate/dist/vue2-animate.min.css');



Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
