import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home'
import Shop from './views/Shop'
import LookBook from './views/LookBook'
import LookBookSingle from './views/LookBookSingle'
import Contact from './views/Contact';
import Cart from './views/Cart';
import Product from './views/Product';

// const Single = { template: '<div>Single</div>' };
// const Shop = { template: '<div>shop</div>' };
// const Category ={ template: '<div>Category page</div>' };
// const LookBook = { template: '<div>LookBook</div>' };
// const LookBookSingle = { template: '<div>LookBook Single</div>' };
// const Contact = { template: '<div>Contact</div>' };
// const Cart = { template: '<div>Cart</div>' };

Vue.use(Router);

export default new Router({
    routes: [
        { path: '/', component: Home, name: 'home'},
        { path: '/product/:url', component: Product, name: 'product' },
        { path: '/shop', component: Shop, name: 'shop' },
        { path: '/shop/:name', component: Shop, name: 'category' },
        { path: '/look-book/', component: LookBook, name: 'lookBook' },
        { path: '/look-book/:url', component: LookBookSingle, name: 'lookBookSingle' },
        { path: '/contact', component: Contact, name: 'contact' },
        { path: '/cart', component: Cart, name: 'cart' },

    ],
    mode: 'history',
    scrollBehavior () {
        return { x: 0, y: 0 }
    }
})