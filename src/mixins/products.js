export default {
    data(){
        return{
            productSizes: [
                {id: 1, label: 'S'},
                {id: 2, label: 'M'},
                {id: 3, label: 'L'},
                {id: 4, label: 'XL'},
                {id: 5, label: '2XL'},
                {id: 6, label: '3XL'},
            ],
            productMale: [
                {id:1, title: 'Women'},
                {id:2, title: 'Men'},
                {id:3, title: 'Children'}
            ],
            productsDemo: [
                {id: 1, name: 'T-Shirt', male: 1, url:'t-shirt-1', sizes: [1, 2, 4], key: 'AT 34563', price: 3000, images:{
                        main: 'new18_w.jpg',
                        cover: 'new1_w.jpg',
                        thumbnail: 'new18_w.jpg',
                        gallery: [
                            {id:1, url: 'new1_w.jpg', class: 'img-half'},
                            {id:2, url: 'new2_w.jpg', class: 'img-half'},
                            {id:3, url: 'new22_w_v.jpg', class: ''},
                            {id:4, url: 'new3_w.jpg', class: ''}
                        ]
                    }},
                {id: 2, name: 'Cap', male: 1, url:'cap-1', sizes: [3, 4, 5], key: 'DB 86564', price: 2300, images:{
                        main: 'new2_w.jpg',
                        cover: 'new3_w.jpg',
                        thumbnail: 'new2_w.jpg',
                        gallery: [
                            {id:1, url: 'new4_w_v.jpg', class: ''},
                            {id:2, url: 'new11_w.jpg', class: 'img-half'},
                            {id:3, url: 'new12_w.jpg', class: 'img-half'},
                        ]
                    }},
                {id: 3, name: 'Jacket', male: 2, url:'jacket-1', sizes: [1, 3, 4, 6], key: 'CT 44568', price: 1500, images:{
                        main: 'new35_m.jpg',
                        cover: 'new38_m.jpg',
                        thumbnail: 'new25_m.jpg',
                        gallery: [
                            {id:1, url: 'new40_m.jpg', class: ''},
                            {id:2, url: 'new38_m.jpg', class: 'img-half'},
                            {id:3, url: 'new41_m.jpg', class: 'img-half'},
                        ]
                    }},
                {id: 4, name: 'Куртка', male: 2, url:'jacket-2', sizes: [1, 2, 4], key: 'CB 18568', price: 2200, images:{
                        main: 'new30_m.jpg',
                        cover: 'new40_m.jpg',
                        thumbnail: 'new30_m.jpg',
                        gallery: [
                            {id:1, url: 'new40_m.jpg', class: ''},
                            {id:2, url: 'new38_m.jpg', class: 'img-half'},
                            {id:3, url: 'new41_m.jpg', class: 'img-half'},
                        ]
                    }},
                {id: 5, name: 'Батник', male: 2, url:'jacket-3', sizes: [3, 4, 6], key: 'VV 44568', price: 920, images:{
                        main: 'new40_m.jpg',
                        cover: 'new30_m.jpg',
                        thumbnail: 'new40_m.jpg',
                        gallery: [
                            {id:1, url: 'new30_m.jpg', class: ''},
                            {id:2, url: 'new38_m.jpg', class: 'img-half'},
                            {id:3, url: 'new41_m.jpg', class: 'img-half'},
                        ]
                    }},
                {id: 6, name: 'Jacket 2', male: 2, url:'jacket-4', sizes: [1,2, 3, 4, 5, 6], key: 'KL 68545', price: 1300, images:{
                        main: 'new7_m.jpg',
                        cover: 'new41_m.jpg',
                        thumbnail: 'new7_m.jpg',
                        gallery: [
                            {id:1, url: 'new7_m.jpg', class: ''},
                            {id:2, url: 'new8_m.jpg', class: ''},
                        ]
                    }},
                {id: 7, name: 'Jacket 3', male: 2, url:'jacket-5', sizes: [1, 3, 4, 6], key: 'CT 44568', price: 1500, images:{
                        main: 'new8_m.jpg',
                        cover: 'new7_m.jpg',
                        thumbnail: 'new8_m.jpg',
                        gallery: [
                            {id:2, url: 'new8_m.jpg', class: 'img-half'},
                            {id:3, url: 'new7_m.jpg', class: 'img-half'},
                        ]
                    }},
                {id: 8, name: 'Куртка 2', male: 2, url:'jacket-6', sizes: [4, 6], key: 'CT 33562', price: 600, images:{
                        main: 'new32_m.jpg',
                        cover: 'new33_m.jpg',
                        thumbnail: 'new32_m.jpg',
                        gallery: [
                            {id:1, url: 'new40_m.jpg', class: ''},
                            {id:2, url: 'new38_m.jpg', class: 'img-half'},
                            {id:3, url: 'new41_m.jpg', class: 'img-half'},
                        ]
                    }},
                {id: 9, name: 'Shorts', male: 2, url:'shorts-1', sizes: [3, 4, 5], key: 'MV 41268', price: 850, images:{
                        main: 'new34_m.jpg',
                        cover: 'new35_m.jpg',
                        thumbnail: 'new34_m.jpg',
                        gallery: [
                            {id:1, url: 'new34_m.jpg', class: 'img-half'},
                            {id:2, url: 'new35_m.jpg', class: 'img-half'},
                            {id:3, url: 'new36_m.jpg', class: 'img-half not-padding-left'},
                            {id:3, url: 'new37_m.jpg', class: 'img-half'},
                        ]
                    }},
                {id: 10, name: 'Shorts 2', male: 2, url:'shorts-2', sizes: [2, 3, 5], key: 'TF 32414', price: 880, images:{
                        main: 'new36_m.jpg',
                        cover: 'new24_m.jpg',
                        thumbnail: 'new36_m.jpg',
                        gallery: [
                            {id:2, url: 'new37_m.jpg', class: 'img-half'},
                            {id:3, url: 'new34_m.jpg', class: 'img-half'},
                            {id:1, url: 'new36_m.jpg', class: ''},
                            {id:3, url: 'new35_m.jpg', class: 'img-half not-padding-left'},
                            {id:3, url: 'new24_m.jpg', class: 'img-half'},
                        ]
                    }},
                {id: 11, name: 'Shorts 3', male: 2, url:'shorts-3', sizes: [1, 3, 4, 6], key: 'BV 14562', price: 960, images:{
                        main: 'new37_m.jpg',
                        cover: 'new36_m.jpg',
                        thumbnail: 'new37_m.jpg',
                        gallery: [
                            {id:1, url: 'new37_m.jpg', class: ''},
                            {id:2, url: 'new36_m.jpg', class: 'img-half'},
                            {id:3, url: 'new24_m.jpg', class: 'img-half'},
                            {id:1, url: 'new35_m.jpg', class: ''}
                        ]
                    }},
                {id: 12, name: 'T-Shirt 3', male: 1, url:'t-shirt-3', sizes: [1, 3, 4], key: 'TT 12563', price: 600, images:{
                        main: 'new26_w.jpg',
                        cover: 'new27_w.jpg',
                        thumbnail: 'new26_w.jpg',
                        gallery: [
                            {id:1, url: 'new26_w.jpg', class: 'img-half'},
                            {id:2, url: 'new27_w.jpg', class: 'img-half'},
                            {id:3, url: 'new22_w_v.jpg', class: ''},
                            {id:4, url: 'new3_w.jpg', class: ''}
                        ]
                    }},
                {id: 13, name: 'Куртка', male: 1, url:'kurtka-1', sizes: [1, 3, 4], key: 'MG 30595', price: 800, images:{
                        main: 'new23_w.jpg',
                        cover: 'new21_w.jpg',
                        thumbnail: 'new23_w.jpg',
                        gallery: [
                            {id:1, url: 'new23_w.jpg', class: 'img-half'},
                            {id:2, url: 'new21_w.jpg', class: 'img-half'},
                            {id:3, url: 'new20_w_v.jpg', class: ''},
                            {id:4, url: 'new22_w_v.jpg', class: ''}
                        ]
                    }},
                {id: 14, name: 'Батник 2', male: 1, url:'batnik-2', sizes: [1, 3, 4], key: 'AE 294956', price: 940, images:{
                        main: 'new17_w.jpg',
                        cover: 'new16_w.jpg',
                        thumbnail: 'new17_w.jpg',
                        gallery: [
                            {id:1, url: 'new17_w.jpg', class: ''},
                            {id:2, url: 'new16_w.jpg', class: 'img-half'},
                            {id:3, url: 'new18_w.jpg', class: 'img-half'},
                            {id:4, url: 'new15_w.jpg', class: ''}
                        ]
                    }},
                {id: 15, name: 'Батник 3', male: 1, url:'batnik-3', sizes: [1, 3, 4], key: 'RE 658956', price: 940, images:{
                        main: 'new11_w.jpg',
                        cover: 'new12_w.jpg',
                        thumbnail: 'new11_w.jpg',
                        gallery: [
                            {id:1, url: 'new11_w.jpg', class: ''},
                            {id:2, url: 'new6_w_v.jpg', class: ''},
                            {id:3, url: 'new12_w.jpg', class: 'img-half'},
                            {id:4, url: 'new23_w.jpg', class: 'img-half'},
                        ]
                    }},
            ],
        }
    },
    methods: {
        getProductByUrl: function (url) {
            for(let i =0; i < this.$store.state.products.length; i++){
                if (this.$store.state.products[i].url === url){
                    return this.$store.state.products[i];
                }
            }
        },
        getProducts: function(male = 0, count = 6){
            let products = [];
            for (let i = 0; i < this.$store.state.products.length; i++){
                if (products.length === count) break;

                let prod = this.$store.state.products[i];
                if (prod.male === male || male === 0){
                    products.push(prod)
                }
            }

            return products;
        },
        getMoreProducts: function (startId = 0, male = 1, count = 6) {
            let products = [];
            for (let i = 0; i < this.$store.state.products.length; i++){
                if (products.length === count) break;

                let prod = this.$store.state.products[i];
                if (startId < prod.id && (prod.male === male || male === 0)){
                    products.push(prod)
                }
            }

            if(products.length < count){
                for (let i = 0; i < this.$store.state.products.length; i++){
                    if (products.length === count) break;

                    let prod = this.$store.state.products[i];
                    if (prod.male === male){
                        products.push(prod)
                    }
                }
            }

            return products;
        },
        getProductSizeData(size_id){
            let result = 'Size not found';

            this.$store.state.productsSizes.forEach(function (el) {
                if(el.id === size_id) result = el;
            });

            return result
        },
        getProductSizeDataInCart(product_id){
            let that = this;
            let result = 'Size not found';

            this.$store.state.cart.products.forEach(function (el) {
                if(el.product_id === product_id) result = that.getProductSizeData(el.size_id);
            });

            return result;
        },
        isProductInCart: function (product_id) {
            let result = false;

            this.$store.state.cart.products.forEach((el)=>{
                if (el.product_id === product_id) result = true;
            });

            return result;
        },
        removeProduct: function (product_id) {
            let products = [];

            this.$store.state.cart.products.forEach((el)=>{
                if (el.product_id !== product_id) products.push(el);
            });

            this.$store.commit('setCartProducts', products);
        }
    }
}